# -*- coding: utf-8 -*-

"""
Sensors to read values from...ZZZ
"""
import abc
import logging
from typing import List, NamedTuple

from hw.pi_mgr import PiBUSManager
from output import OutputObserverFactory, SensorOutputObserver

logger = logging.getLogger(__name__)


class SensorInterface(abc.ABC):
    """ Sensors interface."""

    def __init__(self, sensor_id: dict) -> None:
        """ Initialize the sensor object."""

        self._observers: List[SensorOutputObserver] = []
        self._type: str = ""
        self._identity: dict = sensor_id  # dictionary holds attribute for PiBUSManager or other object

        for obs_config in sensor_id["observers"]:
            self._observers.append(OutputObserverFactory.create_observer())

    @property
    @abc.abstractmethod
    def type(self):
        """ Abstract property of the type of sensor."""
        raise NotImplementedError

    @abc.abstractmethod
    def check_required_config(self):
        """ Ensure any specific hardware requisite for sensor is configured."""
        raise NotImplementedError

    @property
    def identity(self):
        return self._identity

    def read(self, send_to_observers: bool = True) -> NamedTuple:
        """ Read a sensor for measurement.
        :param send_to_observers: Indicator to send measure to list of output sensor observers, default True.
        :type send_to_observers: bool
        :returns List or a single named tuple of the measurements.
        """

        measure: NamedTuple = self.specific_read()

        logger.debug(f"Value from sensor {self} is {measure}")
        logger.debug(f"Number of observers is {len(self._observers)}")

        if send_to_observers:
            for observer in self._observers:
                observer.send(measure)

        return measure

    @abc.abstractmethod
    def specific_read(self):
        """ Sensor specific read. """
        raise NotImplementedError

    def add_observer(self, observer: 'SensorOutputObserver') -> None:
        """ Add a observer to list of observers."""
        self._observers.append(observer)

    def add_hardware_config(self, key, value) -> None:
        """ Adds a hardware configuration item to sensor identity. """

        if "hw" not in self._identity.keys():
            self._identity["hw"] = {}

        self._identity["hw"][key] = value

        logger.debug(f"added hw config: {self._identity['hw']}")

        return None


class MoistureSensor(SensorInterface):
    """ Sensor reading and recording moisture in soil."""

    def specific_read(self) -> NamedTuple:
        """ Read the moisture sensor (right now just ADS1115 hw interface)"""

        # todo(banderson) do we want to generalize to any hardware bus manager? (e.g. arduino, nivida, some genric
        #  system?)
        return PiBUSManager.read_analog_in(self.identity)

    _instance: 'MoistureSensor' = None  # want a single management object for all moisture sensors too
    _hw_configs: dict = {}

    def __new__(cls, *args, **kwargs):

        logger.info(f"Creating or getting MoistureSensor singleton.")

        # todo(banderon) rethink model of master mangement singleton object
        if not cls._instance:
            cls._instance = object.__new__(cls)

        # still creating a new instance of a moisture sensor
        return super(MoistureSensor, cls).__new__(cls)

    def __init__(self, sensor_id: dict):

        super(MoistureSensor, self).__init__(sensor_id)
        self._type = "MoistureSensor"

        if not self.check_required_config():
            self.config_pi()

        # todo(banderson) best place to configure HW is creation of SensorObject instance?
        PiBUSManager.add_analog_in(self)

    @property
    def type(self):
        return self._type

    @staticmethod
    def check_required_config(**kwargs):
        """ Ensure hardware is setup.
        :param **kwargs:
        """

        # todo(banderson) need to model valid hardware configuration for a 'moisture sensor'
        #  this includes the actual moisture sensor and if analog or digital (yes/no level)
        #  mode, if analog also need ADC

        adc_ind: bool = PiBUSManager.is_config("ads1115")
        i2c_ind: bool = PiBUSManager.is_config("i2c")

        return adc_ind and i2c_ind

    def config_pi(self):
        """ Configure Raspberry Pi. """

        logger.info("Configuring the Pi BUS Manager for a Moisture sensor.")

        pi_bus_mgr: PiBUSManager = PiBUSManager()

        #pydevd_pycharm.settrace("10.0.0.116", port=21000, stdoutToServer=True, stderrToServer=True)
        pi_bus_mgr.config_i2c()
        kwargs = {}
        if "gain" in self.identity['config'].keys():
            kwargs["gain"] = self.identity["config"]["gain"]

        pi_bus_mgr.config_ads1115(**kwargs)


class DigitalOutput(SensorInterface):
    """ Generic digital output sensor. """

    def __init__(self, config: dict) -> None:
        """ Initialize instance of a generic digital output.
        :param config: Configuration properties for the sensor.
        :type config: dict
        :returns: Nothing, creates the instance."""

        super(DigitalOutput, self).__init__(config)
        self._type = "DigitalOutput"
        self._channel = self.identity["config"]["pin_num"]

    @property
    def type(self):
        return self._type

    def check_required_config(self):
        pass

    def specific_read(self):
        logger.warning("DigitalOutput shouldn't read.")
        raise UserWarning("DigitalOutput shouldn't read.")

    def set_high(self):
        """ Set the output signal to high."""
        PiBUSManager.config_gpio_pin_high(self._channel)

    def set_low(self):
        """ Set the output signal to low. """
        PiBUSManager.config_gpio_pin_low(self._channel)

    def set_opposite(self):
        """ Set the opposite output signal state."""
        PiBUSManager.config_gpio_pin_opposite(self._channel)

    def cleanup(self):
        """ Cleanup individual sensor instance resources. """
        PiBUSManager.cleanup_gpio(self._channel)

    def get_state(self) -> NamedTuple:
        """ Get the current state of the output pin.
        :returns: Integer if high (1) or low (0).
        :rtype: int"""

        return PiBUSManager.read_digital_in(self._channel)


