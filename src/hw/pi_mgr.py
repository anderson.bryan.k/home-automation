# -*- coding: utf-8 -*-

"""
Raspberry Pi BUS manager. Responsible for managing and interacting with hardware components.
"""
import logging
from collections import namedtuple
from typing import Dict, NamedTuple, List, Union

import board
import busio
from adafruit_ads1x15.analog_in import AnalogIn
import adafruit_ads1x15.ads1115 as ads1115
import RPi.GPIO as GPIO
from hw.sensors import SensorInterface
from utils import exit_program

logger = logging.getLogger(__name__)


class PiBUSManager(object):
    """ Pi BUS Manager to help manage and use various BUS communication. """

    # _instance: 'PiBUSManager' = None  # zzz...type hinting forward reference need single quotes
    _instance = None

    ADS_11x5_MEASURE = namedtuple("ADS_11x5_MEASURE", ["value", "voltage"])
    GPIO_MEASURE = namedtuple("GPIO_MEASURE", ["pin", "state"])

    def __new__(cls, *args, **kwargs):
        """ Called to create a new instance of class."""
        logger.info(f"Inside of new {cls.__name__}")
        if not cls._instance:
            cls._instance = super(PiBUSManager, cls).__new__(cls, *args, **kwargs)

        return cls._instance

    def __init__(self) -> None:
        """ Initialize the instance of the BUS manager, not this uses the singleton design. """

        logger.info(f"Initiating PiBUSManager, id: {id(self)}")

        # todo(banderson) redesign management of hardware configuration, maybe some dict like object
        #  keys are hardware NSNs (or something) and values are specific objects?
        if not hasattr(self, "_ads"):
            # probably shouldn't of went w/ __new__, makes thing complex, but cool thing to learn
            logger.debug(f"creating attributes for {self.__str__()} ")
            self._ads: ads1115.ADS1x15 = None
            self._ads_channels: dict = {}
            self._i2c: busio.I2C = None

    @classmethod
    def config_i2c(cls) -> None:
        """ Configure the I2C. """

        # todo(banderson) refactor to class variables
        pi_bus_mgr = PiBUSManager()
        pi_bus_mgr._i2c = busio.I2C(board.SCL, board.SDA)

    @classmethod
    def config_ads1115(cls, **kwargs) -> None:
        """ Configure a ADS1115 hardware with the I2C communication BUS.
        :param **kwargs: Key word arguments for ADS.
        """

        pi_bus_mgr: PiBUSManager = PiBUSManager()
        # todo(banderson) is it possible to have multiple ADS11x5 hw configured on a single PI?
        #   do we want to have a single process controlling multiple ads111x5?

        if not pi_bus_mgr._i2c:
            logger.error("I2C is not instantiated.")
            raise UserWarning("I2c not instantiated.")

        logger.debug(f"kwargs: {kwargs}")
        pi_bus_mgr._ads = ads1115.ADS1115(pi_bus_mgr._i2c, **kwargs)
        self_ads_channels: Dict[int, AnalogIn] = {}

    @classmethod
    def config_gpio(cls, board_mode: bool = True) -> None:
        """ Configure the GPIO . """

        # only one instance of GPIO board should be used, if the mode is set, it's already be initalized
        if not GPIO.getmode():

            if board_mode:
                GPIO.setmode(GPIO.BOARD)
            else:
                GPIO.setmode(GPIO.BCM)

    @classmethod
    def config_gpio_pin(cls, channel: int, output_ind: bool) -> None:
        """ Configure single GPIO pin. """

        if GPIO.getmode():

            if output_ind:
                GPIO.output(channel)

        else:
            logger.error("GPIO is not configured yet.")
            exit_program(1)



    @classmethod
    def cleanup_gpio(cls, channel: Union[int, List[int]] = None) -> None:
        """ Clean function for GPIO, if channel specified, all is cleaned up.
        :param channel: Channel or channels to clean up.
        :type channel: Union[int, List[int]]
        :returns: Nothing
        """
        pass

    @classmethod
    def is_config(cls, what: str) -> bool:
        """ Helper method to determine if something is configured.
        :param what: The name of the thing to check if configured.
        :returns Boolean if something is configured.
        """

        pi_mgr = PiBUSManager()

        # todo(banderson) have ENUM of all hardware somewhere
        if what == "ads1115":
            if pi_mgr._ads:
                return True
            else:
                return False
        elif what == "i2c":
            if pi_mgr._i2c:
                return True
            else:
                return False
        elif what == "gpio":
            if GPIO.getmode():
                return True
            else:
                # todo(banderson) try implementing exceptions and to have some manager configure and retry...
                logger.warning("GPIO board not configured yet.")
                return False
        else:
            logger.error(f"Don't know the what type ({what}) is configured.")
            exit_program(1)

    @classmethod
    def add_analog_in(cls, sensor: SensorInterface) -> None:
        """
        Add a input (i.e. read) channel to the BUS instance. If channel is
        already in use, returns false.
        :param sensor: Pin number on ADS chip to read measure from.
        :returns: Nothing, configures the sensor object.
        :rtype: None
        """
        # todo(banderson) understand ADS11x5 diff channel better and add functionality here
        # todo(banderson) rethink design pattern for how to manage ADS 111x5 channels

        pi_bus_mgr: PiBUSManager = PiBUSManager()

        if not pi_bus_mgr._ads:
            logger.warning("ADS object not initiated yet.")
            raise UserWarning("ADS object not initiated yet.")

        if len(pi_bus_mgr._ads_channels.keys()) == 0:
            index = 0
        else:
            # todo(banderson) need logic to ensure config isn't already in dictionary
            index = max(pi_bus_mgr._ads_channels.keys()) + 1

        sensor.add_hardware_config("channel_index", index)
        pi_bus_mgr._ads_channels[index] = AnalogIn(pi_bus_mgr._ads, sensor.identity["config"]["pos_pin"])

        logger.debug(f"Add ads channel {index}")

        return None

    @staticmethod
    def read_analog_in(sensor_identity: dict) -> NamedTuple:
        """ Reads the analog channel. (potentially many configs for analog?)
        :param sensor_identity: Dictionary of configuration to read.
        :returns: NamedTuple of the value and voltage of the analog.
        """

        # todo(banderson) refactor how to pass message to read/activating/event a sensor
        # right now, only have ads11x5 analog input, and using 1 pin (not diff mode)...just need pos pin
        channel_index = sensor_identity["hw"]["channel_index"]

        pi_bus_mgr: PiBUSManager = PiBUSManager()

        if channel_index not in pi_bus_mgr._ads_channels.keys():
            logger.error(f"Channel index: {channel_index} not in ADS channel keys: {pi_bus_mgr._ads_channels}")
            raise UserWarning("Channel index not in channel keys.")

        value = pi_bus_mgr._ads_channels[channel_index].value
        voltage = pi_bus_mgr._ads_channels[channel_index].voltage

        return PiBUSManager.ADS_11x5_MEASURE(value, voltage)

    # todo(banderson) need to more cleanly define static vs class methods. PiBUSManager is a singleton
    #  design, does it matter for this pattern?
    @classmethod
    def config_gpio_pin_high(cls, pin_num: int) -> None:
        """ Configures the GPIO pin's state to high.
        :param pin_num: ID of the pin according to how it was configured (BOARD vs BCM).
        :type pin_num: int
        :returns: Nothing, configures the GPIO pin."""

        if not cls.is_config("gpio"):
            exit_program(1)

        GPIO.output(pin_num, GPIO.HIGH)

    @staticmethod
    def config_gpio_pin_low(pin_num: int) -> None:
        """ Configure GPIO's pin state to low.
        :param pin_num: ID of the pin according to how it was configured (BOARD vs BCM).
        :type pin_num: int
        :returns: Nothing, configures the GPIO pin state."""

        if not PiBUSManager.is_config("gpio"):
            exit_program(1)

        GPIO.output(pin_num, GPIO.LOW)

    @staticmethod
    def config_gpio_pin_opposite(pin_num) -> None:
        """ Configures the GPIO pin's state to the opposite state it's currently.
        :param pin_num: ID of the pin according to how it was configured (BOARD vs BCM).
        :type pin_num: int
        :returns: Nothing, configures the GPIO pin state. """

        if not PiBUSManager.is_config("gpio"):
            exit_program(1)

        GPIO.output(pin_num, not GPIO.input(pin_num))

    # todo(banderson) consider creating an object for sensor identity vs just a object
    @staticmethod
    def read_digital_in(sensor_identity: dict) -> NamedTuple:
        """ Reads the digital channel.
        :param sensor_identity: Properties that uniquely ID the sensor.
        :type sensor_identity: dict
        :returns: Named tuple of the digital input signal.
        :rtype: NamedTuple
        :"""

        logger.debug(f"Reading digital input for sensor {sensor_identity['id']}")

        # todo(banderson) need some way to read instantly or callback or blocking event detection
        pin = sensor_identity["config"]["pin_num"]

        return GPIO.input(pin)
