# -*- file encoding: utf-8 -*-
"""
Module covers recording/writing sensor outputs.
"""
import abc
import datetime
import logging
import ssl
import time
from enum import Enum
from typing import NamedTuple
import paho.mqtt.client as mqtt
import jwt
import psycopg2
from psycopg2._json import Json

from utils import exit_program

logger = logging.getLogger("basic")


class OutputObserverType(Enum):
    """ Enumerate list of various types of outputs."""
    UNKNOWN = -1
    CONSOLE = 0
    MQTT = 1
    POSTGRES = 2


class SensorOutputObserver(abc.ABC):
    """ Interface for defining sensor observers, or places where sensors send measures to."""

    def __init__(self, identity: dict) -> None:
        """ Initialize the sensor observer. """
        self._identity = identity

    @property
    def identity(self):
        return self._identity

    @abc.abstractmethod
    def send(self, measure: NamedTuple):
        """ Send the measure to the observer.
        :param measure: Measurement of the sensor to send to observer.
        :type measure: NamedTuple
        """
        raise NotImplementedError


class ConsoleOutputObserver(SensorOutputObserver):
    """ Output sensor output to the console. """

    _instance: 'ConsoleOutputObserver' = None

    def __new__(cls, *args, **kwargs):

        if cls._instance is None:  # todo(banderson) None chacking like this or if not <var>?
            cls._instance = super(ConsoleOutputObserver, cls).__new__(cls)
            cls._instance._initialized = False

        return cls._instance

    def __init__(self, identity: dict):
        """ Need a single instance for interface, use singleton. """

        super().__init__(identity)

        # in console output don't really need to go beyond this line, but example if we need more setup, this is
        # how could potentially look like.
        if self._initialized:
            return

        # really trippy of calling attribute before being set/defined, but example of another singleton implementation
        self._initialized = True

    def send(self, measure: NamedTuple) -> None:
        """ Write the sensor output to the console.
        :param measure: Measure value to write to console.
        :type measure: NamedTuple
        :returns: Nothing to return, just write to console.
        :rtype: None
        """

        logger.debug("About to send data to an sensor observer.")
        print(measure)


class MqttOutputObserver(SensorOutputObserver):
    """ MQTT output observer, sends to MQTT broker. """

    def __init__(self, identity: dict) -> None:
        """ Create an instance to a mqtt broker."""
        super().__init__(identity)

        logger.debug("Creating MQTT output observer.")

        self.connect_mqtt()

    def connect_mqtt(self) -> None:
        """ Create JWT for MQTT authentication, set in client, and make connection."""

        logger.debug("Connecting MQTT client.")

        # currently, the only MQTT used is on Google cloud...this is standard format
        project_id: str = self.identity["config"]["project_id"]
        region: str = self.identity["config"]["region"]
        registry_id: str = self.identity["config"]["registry_id"]
        self._device_id: str = self.identity["config"]["device_id"]

        self._client_id: str = f"projects/{project_id}/locations/{region}/registries/{registry_id}"
        self._client_id += f"/devices/{self._device_id}"

        logger.debug(f"Client id: {self._client_id}")
        self._mqtt_client = mqtt.Client(client_id=self._client_id)

        # aud of the message should be set to the GCP project id
        message = {"iat": datetime.datetime.utcnow(),
                   "exp": datetime.datetime.utcnow() + datetime.timedelta(seconds=
                                                                          self.identity["config"]["jwt_secs_to_expire"]
                                                                          ),
                   "aud": project_id}

        logger.debug(f"pv key: {self.identity['config']['private_key_filepath']}")
        with open(self.identity["config"]["private_key_filepath"], "r") as f:
            # todo(banderson) best practice of private key in runtime mem or read/gc?
            self._private_key = f.read()

        token = jwt.encode(message, self._private_key, algorithm="RS256")

        # MAKE SURE USERNAME IS 'unused' AND NOT NONE!!!
        self._mqtt_client.username_pw_set(username="unused", password=token)

        self._mqtt_client.tls_set(ca_certs=self.identity["config"]["ca_certs_filepath"],
                                  tls_version=ssl.PROTOCOL_TLSv1_2)

        self._mqtt_client.on_connect = self.on_connect
        self._mqtt_client.on_publish = self.on_publish

        self._mqtt_host: str = self.identity["config"]["host"]
        self._mqtt_port: int = self.identity["config"]["port"]

        self._mqtt_client.connect(self._mqtt_host, self._mqtt_port)

        return None

    def on_connect(self, client, userdata, flags, rc):
        logger.debug(f"Connected with result code: {str(rc)}")
        logger.debug(f"Connect comment: {mqtt.connack_string(rc)}")


    def on_publish(self, client, userdata, mid):
        logger.debug("message published")

    def send(self, measure: NamedTuple):
        """ Publish a message to the MQTT broker. """

        # todo(banderson) refactor for checking if client is still valid and reconnect if needed
        # todo(banderson) refactor format of measure and quality of service config
        message_info = self._mqtt_client.publish(f"/devices/{self._device_id}/events", str(measure), qos=1)
        #message_info = self._mqtt_client.publish(f"/devices/{self._device_id}/events", "test", qos=1)

        logger.debug(f"sent data to mqtt: {message_info}")
        logger.debug(f"Message is published: {message_info.is_published()}")
        logger.debug(f"Message result: {message_info.rc}")

        for i in range(0, 2):
            logger.debug(f"sleep and loop {i}")
            time.sleep(1)
            self._mqtt_client.loop()


class PostgresOutputObserver(SensorOutputObserver):
    """ Write to postgres database of the sensor output. """

    def __init__(self, identity: dict) -> None:
        """ Initialize the Postgres instance. """
        
        super(PostgresOutputObserver, self).__init__(identity)

        logger.debug("Creating a Postgres output observer.")

        self._conn = psycopg2.connect(**self.identity["config"]["dns_params"])

    def clean(self):
        """ Close any connections. """
        # in the future we could have maybe many connections/cursors, ensure they're all closed
        # when the program exits

        self._conn.close()

    def send(self, measure: NamedTuple) -> None:
        """ Write the measure's value to a Postgres database.
        :param measure: Sensor value to record.
        :type measure: NamedTuple"""

        measure: dict = {"value": measure.value,
                         "voltage": measure.voltage}

        logger.debug("Sending measure to postgres database.")
        with self._conn.cursor() as cur:
            # cur.callproc("add_moisture_measure(%(sensor_id)s, %(measure)s)", {"sensor_id": "test", "measure":  Json(measure)})
            cur.callproc("sensors.add_moisture_measure", ["test", Json(measure)])

        self._conn.commit()


class OutputObserverFactory(object):
    """ Factory for creating output observers. """

    @staticmethod
    def create_observer(config: dict) -> SensorOutputObserver:
        """ Creates a sensor output observer to send sensor data to.
        :param config: Object with the observer configuration.
        :type config: dict
        :returns: A specific sensor output observer.
        :rtype: SensorOutputObserver"""

        observer_type: OutputObserverType = OutputObserverFactory.get_observer_type(config["type"])

        if observer_type == OutputObserverType.CONSOLE:
            return ConsoleOutputObserver(config)
        elif observer_type == OutputObserverType.MQTT:
            return MqttOutputObserver(config)
        elif observer_type == OutputObserverType.POSTGRES:
            return PostgresOutputObserver(config)
        else:
            logger.error(f"Don't know observer type {observer_type}.")
            exit_program(1)

    @staticmethod
    def get_observer_type(obs_input_type: str) -> OutputObserverType:
        """ Normalizes the observer input type to a enumerate value.
        :param obs_input_type: Configuration input for the output observer type.
        :type obs_input_type: str
        :returns: A enumerated output observer type.
        :rtype: OutputObserverType
        """

        # in the future we may want general or more specific rules to determine types
        if obs_input_type == "postgres":
            return OutputObserverType.POSTGRES
        elif obs_input_type == "mqtt":
            return OutputObserverType.MQTT
        elif obs_input_type == "console":
            return OutputObserverType.CONSOLE
        else:
            logger.warning(f"Don't know observer input type: {obs_input_type}")
            return OutputObserverType.UNKNOWN
