#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import abc
import argparse
import json
import logging
import os
import signal
import sys
import time
import traceback
from collections import namedtuple
from logging.config import dictConfig
from typing import Union, Dict, NamedTuple, List, Any
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

# todo(banderson) what are common attributes for configuring a sensor? Should this be it's own design
#  or classs?
# todo(banderson) rethinking logging approach, how to setup dynamic module logging (e.g. debug, production, testing).
#  Some classes have their own logger for specific logging and testing functionality? (e.g. debug in complex class
#  and everywhere else left at INFO)
from output import MqttOutputObserver, PostgresOutputObserver

logger = logging.getLogger("file")


class SensorInterface(abc.ABC):
    """ Sensors interface."""

    def __init__(self, sensor_id: dict) -> None:
        """ Initialize the sensor object."""

        self._observers: List[SensorOutputObserver] = []
        self._type: str = ""
        self._identity: dict = sensor_id  # dictionary holds attribute for PiBUSManager or other object

        for obs_config in sensor_id["observers"]:
            if obs_config["type"] == "console":
                self._observers.append(ConsoleOutputObserver(obs_config))
            elif obs_config["type"] == "mqtt":
                self._observers.append(MqttOutputObserver(obs_config))
            elif obs_config["type"] == "postgres":
                logger.debug("Creating Postgres observer.")
                self._observers.append(PostgresOutputObserver(obs_config))
            else:
                logger.error(f"Don't know observer type {obs_config['type']}.")
                exit_program(1)

    @property
    @abc.abstractmethod
    def type(self):
        """ Abstract property of the type of sensor."""
        raise NotImplementedError

    @abc.abstractmethod
    def check_required_config(self):
        """ Ensure any specific hardware requisite for sensor is configured."""
        raise NotImplementedError

    @property
    def identity(self):
        return self._identity

    def read(self, send_to_observers: bool = True) -> NamedTuple:
        """ Read a sensor for measurement.
        :param send_to_observers: Indicator to send measure to list of output sensor observers, default True.
        :type send_to_observers: bool
        :returns List or a single named tuple of the measurements.
        """

        measure: NamedTuple = self.specific_read()

        logger.debug(f"Value from sensor {self} is {measure}")
        logger.debug(f"Number of observers is {len(self._observers)}")

        if send_to_observers:
            for observer in self._observers:
                observer.send(measure)

        return measure

    @abc.abstractmethod
    def specific_read(self):
        """ Sensor specific read. """
        raise NotImplementedError

    def add_observer(self, observer: 'SensorOutputObserver') -> None:
        """ Add a observer to list of observers."""
        self._observers.append(observer)

    def add_hardware_config(self, key, value) -> None:
        """ Adds a hardware configuration item to sensor identity. """

        if "hw" not in self._identity.keys():
            self._identity["hw"] = {}

        self._identity["hw"][key] = value

        logger.debug(f"added hw config: {self._identity['hw']}")

        return None


class MoistureSensor(SensorInterface):
    """ Sensor reading and recording moisture in soil."""

    def specific_read(self) -> NamedTuple:
        """ Read the moisture sensor (right now just ADS1115 hw interface)"""

        # todo(banderson) refactor what is the identity of the sensor and how to put how to read in json configuration
        return PiBUSManager.read_analog_in(self.identity)

    _instance: 'MoistureSensor' = None  # want a single management object for all moisture sensors too
    _hw_configs: dict = {}

    def __new__(cls, *args, **kwargs):

        logger.info(f"Creating or getting MoistureSensor singleton.")

        # todo(banderon) rethink model of master mangement singleton object
        if not cls._instance:
            cls._instance = object.__new__(cls)

        # still creating a new instance of a moisture sensor
        return super(MoistureSensor, cls).__new__(cls)

    def __init__(self, sensor_id: dict):

        super(MoistureSensor, self).__init__(sensor_id)
        self._type = "MoistureSensor"

        if not self.check_required_config():
            self.config_pi()

        # todo(banderson) best place to configure HW is creation of SensorObject instance?
        PiBUSManager.add_analog_in(self)

    @property
    def type(self):
        return self._type

    @staticmethod
    def check_required_config(**kwargs):
        """ Ensure hardware is setup.
        :param **kwargs:
        """

        # todo(banderson) need to model valid hardware configuration for a 'moisture sensor'
        #  this includes the actual moisture sensor and if analog or digital (yes/no level)
        #  mode, if analog also need ADC

        adc_ind: bool = PiBUSManager.is_config("ads1115")
        i2c_ind: bool = PiBUSManager.is_config("i2c")

        return adc_ind and i2c_ind

    def config_pi(self):
        """ Configure Raspberry Pi. """

        logger.info("Configuring the Pi BUS Manager for a Moisture sensor.")

        pi_bus_mgr: PiBUSManager = PiBUSManager()

        #pydevd_pycharm.settrace("10.0.0.116", port=21000, stdoutToServer=True, stderrToServer=True)
        pi_bus_mgr.config_i2c()
        kwargs = {}
        if "gain" in self.identity['config'].keys():
            kwargs["gain"] = self.identity["config"]["gain"]

        pi_bus_mgr.config_ads1115(**kwargs)


class SensorOutputObserver(abc.ABC):
    """ Interface for defining sensor observers, or places where sensors send measures to."""

    def __init__(self, identity: dict) -> None:
        """ Initialize the sensor observer. """
        self._identity = identity

    @property
    def identity(self):
        return self._identity

    @abc.abstractmethod
    def send(self, measure: NamedTuple):
        """ Send the measure to the observer.
        :param measure: Measurement of the sensor to send to observer.
        :type measure: NamedTuple
        """
        raise NotImplementedError


class ConsoleOutputObserver(SensorOutputObserver):
    """ Output sensor output to the console. """

    _instance: 'ConsoleOutputObserver' = None

    def __new__(cls, *args, **kwargs):

        if cls._instance is None:  # todo(banderson) None chacking like this or if not <var>?
            cls._instance = super(ConsoleOutputObserver, cls).__new__(cls)
            cls._instance._initialized = False

        return cls._instance

    def __init__(self, identity: dict):
        """ Need a single instance for interface, use singleton. """

        super().__init__(identity)

        # in console output don't really need to go beyond this line, but example if we need more setup, this is
        # how could potentially look like.
        if self._initialized:
            return

        # really trippy of calling attribute before being set/defined, but example of another singleton implementation
        self._initialized = True

    def send(self, measure: NamedTuple) -> None:
        """ Write the sensor output to the console.
        :param measure: Measure value to write to console.
        :type measure: NamedTuple
        :returns: Nothing to return, just write to console.
        :rtype: None
        """

        logger.debug("About to send data to an sensor observer.")
        print(measure)


class PiBUSManager(object):
    """ Pi BUS Manager to help manage and use various BUS communication."""

    # _instance: 'PiBUSManager' = None  # zzz...type hinting forward reference need single quotes
    _instance = None

    ADS_11x5_MEASURE = namedtuple("ADS_11x5_MEASURE", ["value", "voltage"])

    def __new__(cls, *args, **kwargs):
        """ Called to create a new instance of class."""
        logger.info(f"Inside of new {cls.__name__}")
        if not cls._instance:
            cls._instance = super(PiBUSManager, cls).__new__(cls, *args, **kwargs)

        return cls._instance

    def __init__(self) -> None:
        """ Initialize the instance of the BUS manager, not this uses the singleton design. """

        logger.info(f"Initiating PiBUSManager, id: {id(self)}")

        # todo(banderson) redesign management of hardware configuration, maybe some dict like object
        #  keys are hardware NSNs (or something) and values are specific objects?
        if not hasattr(self, "_ads"):
            # probably shouldn't of went w/ __new__, makes thing complex, but cool thing to learn
            logger.debug(f"creating attributes for {self.__str__()} ")
            self._ads: ADS.ADS1x15 = None
            self._ads_channels: dict = {}
            self._i2c: busio.I2C = None

    @classmethod
    def config_i2c(cls) -> None:
        """ Configure the I2C. """

        # todo(banderson) refactor to class variables
        pi_bus_mgr = PiBUSManager()
        pi_bus_mgr._i2c = busio.I2C(board.SCL, board.SDA)

    @classmethod
    def config_ads1115(cls, **kwargs) -> None:
        """ Configure a ADS1115 hardware with the I2C communication BUS.
        :param **kwargs: Key word arguments for ADS.
        """

        pi_bus_mgr: PiBUSManager = PiBUSManager()
        # todo(banderson) is it possible to have multiple ADS11x5 hw configured on a single PI?
        #   do we want to have a single process controlling multiple ads111x5?

        if not pi_bus_mgr._i2c:
            logger.error("I2C is not instantiated.")
            raise UserWarning("I2c not instantiated.")

        logger.debug(f"kwargs: {kwargs}")
        pi_bus_mgr._ads = ADS.ADS1115(pi_bus_mgr._i2c, **kwargs)
        self_ads_channels: Dict[int, AnalogIn] = {}

    @classmethod
    def is_config(cls, what: str) -> bool:
        """ Helper method to determine if something is configured.
        :param what: The name of the thing to check if configured.
        :returns Boolean if something is configured.
        """

        pi_mgr = PiBUSManager()

        # todo(banderson) have ENUM of all hardware somewhere
        if what == "ads1115":
            if pi_mgr._ads:
                return True
            else:
                return False
        elif what == "i2c":
            if pi_mgr._i2c:
                return True
            else:
                return False
        else:
            logger.error(f"Don't know the what type ({what}) is configured.")
            exit_program(1)

    @classmethod
    def add_analog_in(cls, sensor: SensorInterface) -> None:
        """
        Add a input (i.e. read) channel to the BUS instance. If channel is
        already in use, returns false.
        :param sensor: Pin number on ADS chip to read measure from.
        :returns: Nothing, configures the sensor object.
        :rtype: None
        """
        # todo(banderson) understand ADS11x5 diff channel better and add functionality here
        # todo(banderson) rethink design pattern for how to manage ADS 111x5 channels

        pi_bus_mgr: PiBUSManager = PiBUSManager()

        if not pi_bus_mgr._ads:
            logger.warning("ADS object not initiated yet.")
            raise UserWarning("ADS object not initiated yet.")

        if len(pi_bus_mgr._ads_channels.keys()) == 0:
            index = 0
        else:
            # todo(banderson) need logic to ensure config isn't already in dictionary
            index = max(pi_bus_mgr._ads_channels.keys()) + 1

        sensor.add_hardware_config("channel_index", index)
        pi_bus_mgr._ads_channels[index] = AnalogIn(pi_bus_mgr._ads, sensor.identity["config"]["pos_pin"])

        logger.debug(f"Add ads channel {index}")

        return None

    @staticmethod
    def read_analog_in(sensor_identity: dict) -> NamedTuple:
        """ Reads the analog channel. (potentially many configs for analog?)
        :param sensor_identity: Dictionary of configuration to read.
        :returns: NamedTuple of the value and voltage of the analog.
        """

        # todo(banderson) refactor how to pass message to read/activating/event a sensor
        # right now, only have ads11x5 analog input, and using 1 pin (not diff mode)...just need pos pin
        logger.debug(f"Identity: {sensor_identity.keys()}")
        channel_index = sensor_identity["hw"]["channel_index"]

        pi_bus_mgr: PiBUSManager = PiBUSManager()

        if channel_index not in pi_bus_mgr._ads_channels.keys():
            logger.error(f"Channel index: {channel_index} not in ADS channel keys: {pi_bus_mgr._ads_channels}")
            raise UserWarning("Channel index not in channel keys.")

        value = pi_bus_mgr._ads_channels[channel_index].value
        voltage = pi_bus_mgr._ads_channels[channel_index].voltage

        return PiBUSManager.ADS_11x5_MEASURE(value, voltage)


def parse_args() -> argparse.Namespace:
    """ Parse command line arguments. """

    args_parser = argparse.ArgumentParser(description="Moisture sensor prototype.", epilog="Hopefully it works.")

    # todo(banderson) this should havea default value instead of being required
    args_parser.add_argument("-l", "--log-config-filepath", help="specify the log configuration filepath.",
                             required=True)
    args_parser.add_argument("-c", "--sensor-config-filepath", help="specify the sensor configuration filepath.",
                             default="etc/config_sensor.json")

    parsed_args = args_parser.parse_args()

    return parsed_args


def setup_logging(args: argparse.Namespace) -> None:
    """ Configure and setup logging, log any previous configurations/parameters for historical analysis.
    :param args: Configurations/parameters used.
    :type args: argparse.Namespace
    :returns: Nothing to return, setups logging for the module.
    :rtype: None
     """

    try:
        log_config: dict = json.load(open(args.log_config_filepath, "r"))
        dictConfig(log_config)

    except FileNotFoundError as e1:
        # To turn off the default print traceback I think sys.tracebacklimit = -1, but hopefully it doesn't happen often
        print(f"IOError with the log configuration filepath: {e1}", file=sys.stderr)
        sys.exit(1)

    logger.info(f"log-config-filepath is {args.log_config_filepath}.")
    logger.info(f"sensor-config-filepath is {args.sensor_config_filepath}.")


# todo(banderson) have linux signal package now to recieve signals and change flags in program to change execution
#  logic. Future can have windows version as a different package changing things...abstract the way to change flags
#  in the program..
def my_signal_fn(signal: int, frame) -> None:
    """ Simple linux signal fn"""

    print(f"Received: {signal}")

    return


class ModeInterface(abc.ABC):
    """ Interface to program mode to run."""

    @property
    @abc.abstractmethod
    def name(self):
        """ Name of the specific mode program is running as."""
        raise NotImplementedError

    @abc.abstractmethod
    def run(self, sensors: List[SensorInterface]) -> None:
        """ Run application in a standard way."""
        raise NotImplemented

    @staticmethod
    def get_mode(args: argparse.Namespace) -> 'ModeInterface':
        """ Get the mode to run as.
        :param args: Arguments and configuration at start up.
        :type args: argparse.Namespace
        :returns: Object with logic to run.
        :rtype: ModeInterface
        """

        # todo(banderson) only read the sensor config once
        with open(args.sensor_config_filepath, "r") as f:
            config = json.load(f)
            logger.debug("loaded config")
            mode: str = config["mode"]["type"]

        logger.debug(f"Running {mode}")
        if mode == "singleRun":
            return SingleRunMode()
        elif mode == "simpleSleepMode":
            return SimpleSleepRunMode(args)
        else:
            logger.error(f"Don't know type {mode}.")
            exit_program(1)


class SingleRunMode(ModeInterface):
    """ Just run the configuration and read sensors once. """

    @property
    def name(self):
        return "SingeRunMode"

    def run(self, sensors: List[SensorInterface]) -> None:
        """ Run and read the sensors once. """

        for sensor in sensors:
            sensor.read()


class SimpleSleepRunMode(ModeInterface):
    """ Simple sleep run mode, sleep for configurable amount of time and take measures. """

    def __init__(self, args: argparse.Namespace) -> None:
        """ Initialize the simple sleep run mode.
        :param args: The arguments given on the start of the program.
        :type args: argparse.Namespace
        :returns: Nothing, creates an instance of the class.
        :rtype: None
        """

        # todo(banderson) only read sensor config once
        with open(args.sensor_config_filepath, "r") as f:
            config = json.load(f)

        self._secs_to_sleep = config["mode"]["config"]["secs_to_sleep"]

    @property
    def name(self):
        return "SimpleSleepRunMode"

    def run(self, sensors: List[SensorInterface]) -> None:

        logger.debug(f"Running as {self.name}")

        while True:

            for sensor in sensors:
                sensor.read()

            time.sleep(self._secs_to_sleep)


class ModeContextManager(object):
    """ Helper class to manage the mode the run in. """

    _mode: ModeInterface = None
    _sensor_list: List[dict] = []

    def __init__(self, mode: ModeInterface) -> None:
        """ Initialize the mode context manager, helps configure the structure the program.
        :param mode ModeInterface: Common interface strategy to run the program.
        """

        self._mode = mode
        self._sensor_list = []

    def run(self):
        """ Run the program for the given mode"""
        logger.info(f"Running program as {self._mode.name}")
        self._mode.run(self._sensor_list)

    def start(self, args: argparse.Namespace) -> None:
        """ Start the program in standard way, setup initial configurations here.
        :param args: Arguments to start the program.
        :type args: argparse.Namespace
        :returns: Nothing to return here.
        :rtype: None
        """

        logger.info(f"Starting program for the first time...creating Pi BUS Manager.")

        logger.info("Configuring sensors...")
        # todo(banderson) do test for bad file, catch exception, should also be read once
        sensor_config = json.load(open(args.sensor_config_filepath, "r"))

        for sensor in sensor_config["sensors"]:

            # todo(banderson) refactor config logic to include purpose of sensor (e.g. moisture, pic, humidity...)
            if sensor["hw_type"] == "ads1115":
                self._sensor_list.append(MoistureSensor(sensor))

            else:
                logger.error(f"Unknown hardware type {sensor['hw_type']}")
                exit_program(1)

        logger.info("Starting to run.")
        self.run()
        logger.info("Exiting start.")


def exit_program(status):
    """ Standard way to gracefully exit the program.
    :param status: Integer exit code.
    """

    logger.info("Exiting program.")
    sys.exit(status)


def main(args: argparse.Namespace) -> None:

    run_mode: ModeInterface = ModeInterface.get_mode(args)
    mgr = ModeContextManager(run_mode)
    mgr.start(args)

    exit_program(0)


def main_old(args: argparse.Namespace) -> None:
    """ Main application holder.
    :param args argparse.Namespace: single object of all the app configurations.
    """

    logger.info("Before creating the PiBUSManager.")
    pi_mgr: PiBUSManager = PiBUSManager()
    pi_mgr.config_ads1115()

    logger.info("Creating the analog channel.")
    channel_index = pi_mgr.add_analog_in(args.pos_pin)

    measure: PiBUSManager.ADS_11x5_MEASURE = pi_mgr.read_analog_in(channel_index)
    stmt: str = f"Sensor value: {measure.value} & voltage: {measure.voltage}"
    logger.info(stmt)
    print(stmt)

    print(f"My process id {os.getpid()}")
    print("going to sleep")
    time.sleep(20)
    print("slept for 10 seconds")


if __name__ == "__main__":
    args = parse_args()
    setup_logging(args)

    # todo(banderson) logic for on fly debug config
    #import pydevd_pycharm
    #pydevd_pycharm.settrace("10.0.0.116", port=21000, stdoutToServer=True, stderrToServer=True)

    signal.signal(signal.SIGUSR1, my_signal_fn)
    main(args)
