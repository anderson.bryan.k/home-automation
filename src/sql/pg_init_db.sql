
--// basic requirements for database in here

create or replace procedure ensure_extensions_enabled()
language plpgsql
as $$
declare
begin

    if not EXISTS (select * from pg_extension where extname = 'pgcrypto') then
        raise notice 'Enabling pgcrypto.';
        create extension pgcrypto;
    else
        raise info 'pgcrypto already enabled.';
    end if;

end
$$;

create or replace procedure ensure_roles()
language plpgsql
as $$
declare
    ha_roles text[] := array['ha_user', 'ha_editor', 'ha_admin'];
    ha_role text ;
begin

     <<ha_loop>>
    foreach ha_role in array ha_roles loop
         if not exists(select * from pg_roles where rolname = ha_role) then
             raise notice 'Creating % role', ha_role;
             execute 'create role ' || ha_role;
         end if;
    end loop ha_loop;

end
$$;

create or replace procedure ensure_schemas()
language plpgsql
as $$
declare
begin

    if not exists(select * from information_schema.schemata where schema_name = 'mgmt') then
        raise notice 'Creating the mgmt schema.';
        create schema mgmt;
    end if;

end
$$;



call ensure_extensions_enabled();
drop procedure if exists ensure_extensions_enabled();

call ensure_roles();
drop procedure if exists ensure_roles();

call ensure_schemas();
drop procedure if exists ensure_schemas();



