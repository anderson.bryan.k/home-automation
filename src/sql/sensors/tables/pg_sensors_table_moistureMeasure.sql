--liquibase formatted sql

--changeset bka:zzz_sensors_table_moistureMeasure
drop table if exists sensors.moisture_measure cascade ;
create table sensors.moisture_measure
(
    sensor_id text not null,
    measure jsonb
) inherits (mgmt.prov_annotation)
with (
  oids = false
);

alter table sensors.moisture_measure owner to postgres;
grant all on table sensors.moisture_measure to postgres, ha_admin;
grant select, insert, update on table sensors.moisture_measure to ha_editor;
grant select on table sensors.moisture_measure to ha_user;

comment on table sensors.moisture_measure is 'Sensor table for moisture readings';
insert into mgmt.help (full_name, item_type_code, is_stable, short_description, long_description) values
('sensors.moisture_measure', 'TABLE', false, 'Sensor table for moisture readings', '');

--rollback drop table sensors.moisture_measure cascade ;
--rollback delete from mgmt.help where full_name like 'sensors.moisture_measure';
