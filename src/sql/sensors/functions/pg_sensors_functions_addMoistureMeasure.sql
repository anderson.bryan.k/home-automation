--liquibase formatted sql

--changeset bka:zzz_sensors_functions_addMoistureMeasure splitStatements:false

create or replace function sensors.add_moisture_measure(sensors_id_ text, measure_ jsonb)
returns void as
$body$
begin

    insert into sensors.moisture_measure(sensor_id,   measure) values
                                        (sensors_id_, measure_);

end;
$body$
language plpgsql volatile ;

alter function sensors.add_moisture_measure(sensors_id_ text, measure_ jsonb) owner to postgres;
grant execute on function sensors.add_moisture_measure(sensors_id_ text, measure_ jsonb) to ha_editor, ha_admin;

insert into mgmt.help(full_name, item_type_code, is_stable, short_description, long_description) values
('sensors.add_moisture_measure', 'FUNCTION', false, 'Add moisture sensor measure.', '');
comment on function sensors.add_moisture_measure(sensors_id_ text, measure_ jsonb) is 'Add moisture sensor measure.';

--rollback drop function sensors.add_moisture_measure(text, jsonb) cascade ;
--rollback delete from mgmt.help where full_name like 'sensors.add_moisture_measure';

