--liquibase formatted sql

--changeset bka:zzz_schema_sensors

create schema sensors;

grant all on schema sensors to postgres, ha_admin;
grant usage on schema sensors to ha_editor, ha_user;

--rollback drop schema sensors cascade ;

