--liquibase formatted sql

--changeset bka:ZZZ-mgmt-schema
-- create schema mgmt; --// this needs to be created before so changelog will be in there

grant all on schema mgmt to ha_admin;
grant usage on schema mgmt to ha_editor, ha_user;

--rollback revoke all on schema mgmt from postgres, ha_admin;
--rollback revoke usage on schema mgmt from ha_editor, ha_user;
