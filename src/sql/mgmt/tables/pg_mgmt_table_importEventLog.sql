--liquibase formatted sql

--changeset bka:zzz_mgmt_table_importEventLog

drop table if exists mgmt.import_event cascade ;
create table mgmt.import_event
(
    qa_import_event_timestamp timestamp(6) with time zone not null default (now() at time zone 'utc'),
    constraint mq_iel_pk_qa_id primary key (qa_id),
    constraint mq_iel_fk_review_status foreign key (qa_review_status_code)
        references mgmt.codes_review_status (review_status_code) on update no action on delete no action
) inherits (mgmt.prov_annotation)
with (
    oids=false
);

grant select on table mgmt.import_event to ha_user;
grant all on table mgmt.import_event to ha_editor, ha_admin;

--rollback drop table mgmt.import_event cascade;