--liquibase formatted sql

--changeset bka:zzz_mgmt_table_help

drop table if exists mgmt.help cascade ;
create table mgmt.help
(
    full_name varchar not null,
    short_name varchar default null,
    item_type_code varchar not null,
    is_stable boolean not null,
    short_description varchar not null,
    long_description varchar default null
) with
(
    oids = false
);

alter table mgmt.help add constraint dms_help_full_name_pk primary key (full_name);
alter table mgmt.help add constraint dms_help_item_code_fk foreign key (item_type_code)
  references mgmt.help_item_type_codes (help_item_type_code) match full on update no action on delete no action ;

alter table mgmt.help owner to postgres;
grant all on table mgmt.help to postgres;

comment on table mgmt.help is 'help information on using the database.';

insert into mgmt.help (full_name, item_type_code, is_stable, short_description, long_description) VALUES
                             ('mgmt_schema', 'SCHEMA', true, 'support for documenting database usage', '');

insert into mgmt.help (full_name, item_type_code, is_stable, short_description, long_description) values
                             ('mgmt_schema.help_item_type_codes', 'TABLE', true, 'codes table for database objects', '');

insert into mgmt.help (full_name, item_type_code, is_stable, short_description, long_description) values
                             ('mgmt_schema.help', 'TABLE', false, 'help documentation table.', '');

grant select on mgmt.help to ha_user;
grant select, insert, update on mgmt.help to ha_editor;
grant all on mgmt.help to ha_admin;

--rollback drop table mgmt.help cascade;