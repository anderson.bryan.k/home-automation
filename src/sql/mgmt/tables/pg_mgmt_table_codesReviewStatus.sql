--liquibase formatted sql

--changeset bka:ZZZ_mgmt_table_codesReviewStatus

drop table if exists mgmt.codes_review_status cascade ;
create table mgmt.codes_review_status
(
    review_status_code smallint not null ,
    name varchar not null,
    description varchar not null ,
    constraint mq_crs_pk primary key (review_status_code)
);

alter table mgmt.codes_review_status owner to postgres;

grant all on table mgmt.codes_review_status to postgres, ha_admin;
grant select, insert, update on table mgmt.codes_review_status to ha_editor;
grant select on table mgmt.codes_review_status to ha_user;

insert into mgmt.help (full_name, item_type_code, is_stable, short_description, long_description) VALUES
                             ('mgmt_quality.codes_review_status', 'TABLE', false, 'Codes table of valid review states.', '');

insert into mgmt.codes_review_status (review_status_code, name, description) values (1, 'UNREVIEWED', 'No QA review yet');
INSERT INTO mgmt.codes_review_status (review_status_code, name, description) VALUES (2, 'INPROGRESS', 'QA review in progress.');
INSERT INTO mgmt.codes_review_status (review_status_code, name, description) VALUES (3, 'SUCCESS', 'QA Review completed successfully.');
INSERT INTO mgmt.codes_review_status (review_status_code, name, description) VALUES (4, 'FAILURE', 'QA Review completed unsuccessfully.');

--rollback drop table mgmt.codes_review_status cascade;