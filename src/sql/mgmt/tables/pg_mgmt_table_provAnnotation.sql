--liquibase formatted sql

--changeset bka:ZZZ_mgmt_table_provAnnotation

drop table if exists mgmt.prov_annotation cascade;
create table mgmt.prov_annotation
(
    qa_id uuid not null default gen_random_uuid(),
    qa_review_status_code smallint not null default (1)
) with (
    oids = false
);

alter table mgmt.prov_annotation owner to postgres;
grant all on table mgmt.prov_annotation to postgres;
comment on table mgmt.prov_annotation is 'Provenance annotation, all tuples will have standard UUID.';

insert into mgmt.help(full_name, item_type_code, is_stable, short_description, long_description) values
                     ('mgmt.prov_annotation', 'TABLE', false, 'base provenance table to track UUIDs.', '');

grant select on mgmt.prov_annotation to ha_user;
grant select, insert, update on mgmt.prov_annotation to ha_editor;
grant all on mgmt.prov_annotation to ha_admin;

--rollback drop table mgmt.prov_annotation cascade;
--rollback delete from mgmt.help where full_name = 'mgmt.prov_annotation';

