--liquibase formatted sql

--changeset bka:zzz_mgmt_table_helpItemTypeCodes

drop table if exists mgmt.help_item_type_codes cascade ;
create table mgmt.help_item_type_codes
(
    help_item_type_code varchar not null
) with
(
    oids = false
);

alter table mgmt.help_item_type_codes add constraint dms_help_item_type_code_pk primary key (help_item_type_code);

alter table mgmt.help_item_type_codes owner to postgres;
grant all on table mgmt.help_item_type_codes to postgres;
comment on table mgmt.help_item_type_codes is 'help object codes';

insert into mgmt.help_item_type_codes(help_item_type_code) values ('FUNCTION');
insert into mgmt.help_item_type_codes(help_item_type_code) values ('SCHEMA');
insert into mgmt.help_item_type_codes(help_item_type_code) values ('TABLE');
insert into mgmt.help_item_type_codes(help_item_type_code) values ('VIEW');
insert into mgmt.help_item_type_codes(help_item_type_code) values ('COLUMN');
insert into mgmt.help_item_type_codes(help_item_type_code) values ('ROLE');

grant select on mgmt.help_item_type_codes to ha_user;
grant select, insert, update on mgmt.help_item_type_codes to ha_editor;
grant all on mgmt.help_item_type_codes to ha_admin;


--rollback drop table mgmt.help_item_type_codes cascade;
