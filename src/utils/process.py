# -*- coding: utf-8 -*-

""""
Process module is for supporting the process context logic.
"""
import abc
import argparse
import json
import logging
import sys
import time
from logging.config import dictConfig
from typing import List

from hw.sensors import SensorInterface, MoistureSensor

logger = logging.getLogger(__name__)


class ModeInterface(abc.ABC):
    """ Interface to program mode to run."""

    @property
    @abc.abstractmethod
    def name(self):
        """ Name of the specific mode program is running as."""
        raise NotImplementedError

    @abc.abstractmethod
    def run(self, sensors: List[SensorInterface]) -> None:
        """ Run application in a standard way."""
        raise NotImplemented

    # todo(banderson) make common config class which parses the argparse namespace/json/yaml/other configs and manages
    #  in one spot
    @staticmethod
    def get_mode(args: argparse.Namespace) -> 'ModeInterface':
        """ Get the mode to run as.
        :param args: Arguments and configuration at start up.
        :type args: argparse.Namespace
        :returns: Object with logic to run.
        :rtype: ModeInterface
        """

        # todo(banderson) only read the sensor config once
        with open(args.sensor_config_filepath, "r") as f:
            config = json.load(f)
            logger.debug("loaded config")
            mode: str = config["mode"]["type"]

        logger.debug(f"Running {mode}")
        if mode == "singleRun":
            return SingleRunMode()
        elif mode == "simpleSleepMode":
            return SimpleSleepRunMode(args)
        else:
            logger.error(f"Don't know type {mode}.")
            exit_program(1)


class ModeContextManager(object):
    """ Helper class to manage the mode the run in. """

    _mode: ModeInterface = None
    _sensor_list: List[SensorInterface] = []

    def __init__(self, mode: ModeInterface) -> None:
        """ Initialize the mode context manager, helps configure the structure the program.
        :param mode ModeInterface: Common interface strategy to run the program.
        """

        self._mode = mode
        self._sensor_list = []

    def run(self):
        """ Run the program for the given mode"""
        logger.info(f"Running program as {self._mode.name}")
        self._mode.run(self._sensor_list)

    def start(self, args: argparse.Namespace) -> None:
        """ Start the program in standard way, setup initial configurations here.
        :param args: Arguments to start the program.
        :type args: argparse.Namespace
        :returns: Nothing to return here.
        :rtype: None
        """

        logger.info(f"Starting program for the first time...creating Pi BUS Manager.")

        logger.info("Configuring sensors...")
        # todo(banderson) do test for bad file, catch exception, should also be read once
        sensor_config = json.load(open(args.sensor_config_filepath, "r"))

        for sensor in sensor_config["sensors"]:

            # todo(banderson) refactor config logic to include purpose of sensor (e.g. moisture, pic, humidity...)
            if sensor["hw_type"] == "ads1115":
                self._sensor_list.append(MoistureSensor(sensor))

            else:
                logger.error(f"Unknown hardware type {sensor['hw_type']}")
                exit_program(1)

        logger.info("Starting to run.")
        self.run()
        logger.info("Exiting start.")


class SingleRunMode(ModeInterface):
    """ Just run the configuration and read sensors once. """

    @property
    def name(self):
        return "SingeRunMode"

    def run(self, sensors: List[SensorInterface]) -> None:
        """ Run and read the sensors once. """

        for sensor in sensors:
            sensor.read()


class SimpleSleepRunMode(ModeInterface):
    """ Simple sleep run mode, sleep for configurable amount of time and take measures. """

    def __init__(self, args: argparse.Namespace) -> None:
        """ Initialize the simple sleep run mode.
        :param args: The arguments given on the start of the program.
        :type args: argparse.Namespace
        :returns: Nothing, creates an instance of the class.
        :rtype: None
        """

        # todo(banderson) only read sensor config once
        with open(args.sensor_config_filepath, "r") as f:
            config = json.load(f)

        self._secs_to_sleep = config["mode"]["config"]["secs_to_sleep"]

    @property
    def name(self):
        return "SimpleSleepRunMode"

    def run(self, sensors: List[SensorInterface]) -> None:

        logger.debug(f"Running as {self.name}")

        while True:

            for sensor in sensors:
                sensor.read()

            time.sleep(self._secs_to_sleep)

class ListenRunMode(ModeInterface):
    """ """


def parse_args() -> argparse.Namespace:
    """ Parse command line arguments. """

    args_parser = argparse.ArgumentParser(description="Moisture sensor prototype.", epilog="Hopefully it works.")

    # todo(banderson) this should havea default value instead of being required
    args_parser.add_argument("-l", "--log-config-filepath", help="specify the log configuration filepath.",
                             required=True)
    args_parser.add_argument("-c", "--sensor-config-filepath", help="specify the sensor configuration filepath.",
                             default="etc/config_sensor.json")

    parsed_args = args_parser.parse_args()

    return parsed_args


def setup_logging(args: argparse.Namespace) -> None:
    """ Configure and setup logging, log any previous configurations/parameters for historical analysis.
    :param args: Configurations/parameters used.
    :type args: argparse.Namespace
    :returns: Nothing to return, setups logging for the module.
    :rtype: None
     """

    try:
        log_config: dict = json.load(open(args.log_config_filepath, "r"))
        dictConfig(log_config)

    except FileNotFoundError as e1:
        # To turn off the default print traceback I think sys.tracebacklimit = -1, but hopefully it doesn't happen often
        print(f"IOError with the log configuration filepath: {e1}", file=sys.stderr)
        sys.exit(1)

    logger.info(f"log-config-filepath is {args.log_config_filepath}.")
    logger.info(f"sensor-config-filepath is {args.sensor_config_filepath}.")


# todo(banderson) have linux signal package now to recieve signals and change flags in program to change execution
#  logic. Future can have windows version as a different package changing things...abstract the way to change flags
#  in the program..
def my_signal_fn(signal: int, frame) -> None:
    """ Simple linux signal fn"""

    print(f"Received: {signal}")

    return

def exit_program(status):
    """ Standard way to gracefully exit the program.
    :param status: Integer exit code.
    """

    logger.info("Exiting program.")
    sys.exit(status)

